﻿using UnityEngine;
using System.Collections;

public class CamFollow : Photon.MonoBehaviour 
{

	public PhotonView playerShip;
	private GameObject[] objs;
	
	void Update()
	{
		if (playerShip == null)
		{
			pv ();
		}
		
	}
	
	
	void FixedUpdate()
    {	
    	
		transform.localPosition = new Vector3(playerShip.transform.position.x,transform.position.y, playerShip.transform.position.z);
		transform.localRotation = this.transform.rotation;
		
	}
	
	void pv()
	{
		objs = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject o in objs)
		{
			if (o.GetPhotonView().isMine)
			{
				playerShip = o.GetPhotonView();
			}
		}
	}
}
