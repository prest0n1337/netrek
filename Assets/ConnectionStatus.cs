using UnityEngine;
using System.Collections;
using System;

public class ConnectionStatus : Photon.MonoBehaviour 
{
	public int numOfPlayers;
	
	void Start () 
	{
	}
	
	void Update()
	{
		try
		{
			numOfPlayers = PhotonNetwork.room.playerCount;
		}
		catch(NullReferenceException)
		{
			GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
			numOfPlayers = players.Length;
		}
	}
	
	void OnGUI () 
	{
		GUILayout.Label("Connection status: " + PhotonNetwork.connectionStateDetailed.ToString());
		GUILayout.Label("Number of players: " + numOfPlayers.ToString());
		GUILayout.Label("Ping: " + PhotonNetwork.GetPing().ToString() + "ms");
		
		
	}
}
