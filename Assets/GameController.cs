﻿using UnityEngine;
using System.Collections;

public class GameController : Photon.MonoBehaviour
{
	public GameObject player;
	public PlayerHealth playerHealth;
	public PhotonView gameView;
	public Canvas fedShips;
	public Canvas romShips;
	
	void Start()
	{
		playerHealth = player.GetComponent<PlayerHealth>();
	}
	
	void Update()
	{
		getThisHealth();
	}
	
	void getThisHealth()
	{
		if(playerHealth == null)
		{
			playerHealth = player.GetComponent<PlayerHealth>();
			
		}		
		if(playerHealth.currentPlayerHealth <= 0 && playerHealth.enabled == true)
			{
				playerHealth.alive = false;
				if(player.layer == 11)
				{
					GameObject.FindGameObjectWithTag("FederationCanvas").GetComponent<Canvas>().enabled = true;
				}
				else if (player.layer == 12)
				{
					GameObject.FindGameObjectWithTag("RomulanCanvas").GetComponent<Canvas>().enabled = true;
				}
				PhotonView playerView;
				playerView = player.GetComponent<PhotonView>();
				gameView.RPC("killPlayer", PhotonTargets.All, playerView.viewID);
				Debug.Log("BOOM!" + " " + playerView.viewID);
			}
		
	}
	
	[PunRPC]
	void killPlayer(int playerViewId)
	{
		PhotonNetwork.Destroy(PhotonView.Find(playerViewId));		
		PhotonNetwork.Destroy(this.gameObject.GetPhotonView());		
	}
	
}
