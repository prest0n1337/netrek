﻿using UnityEngine;
using System.Collections;

public class NameChanger : MonoBehaviour 
{
	public PhotonView pV;
	public GameObject gO;

	void Start () 
	{
		gO.name = pV.viewID.ToString();		
	}
	
	
}
