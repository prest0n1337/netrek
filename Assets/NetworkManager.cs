﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class NetworkManager : Photon.MonoBehaviour 
{	
	GameObject player;
	GameObject playerShip;
	public string ship;
	public ConnectionStatus status;
	
	void Start()
	{
		status.enabled = true;
		PhotonNetwork.ConnectUsingSettings("Test");
	}	
			
	void OnJoinedLobby()
	{
		PhotonNetwork.JoinRandomRoom();		
	}
	
	void OnPhotonRandomJoinFailed()
	{
		PhotonNetwork.CreateRoom("Official");
		
	}
	void OnJoinedRoom()
	{		
		if (PhotonNetwork.player.GetTeam().ToString() == "federation")
		{			
			Vector3 spawnFed = new Vector3(Random.Range(2525, 2575), 0 ,Random.Range(-2080, -2130)); 
			player = PhotonNetwork.Instantiate(ship.ToString(), spawnFed, Quaternion.LookRotation(Vector3.forward), 0);
			player.GetComponent<PlayerController>().enabled = true;			
			player.GetComponentInChildren<shotSpawnMover>().enabled = true;			
		}
		
		else if (PhotonNetwork.player.GetTeam().ToString() == "romulan")
		{
			Vector3 spawnRom = new Vector3(Random.Range(2525, 2575), 0 ,Random.Range(1600, 1650)); 
			player = PhotonNetwork.Instantiate(ship.ToString(), spawnRom, Quaternion.LookRotation(Vector3.back), 0);
			player.GetComponent<PlayerController>().enabled = true;				
			player.GetComponentInChildren<shotSpawnMover>().enabled = true;			
		}
		GameObject gameController = PhotonNetwork.Instantiate("GameController", Vector3.zero, Quaternion.identity, 0);
		gameController.GetComponent<GameController>().enabled = true;
		gameController.GetComponent<GameController>().player = player;
		
	}
}
