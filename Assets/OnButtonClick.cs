﻿using UnityEngine;
using System.Collections;

public class OnButtonClick : Photon.MonoBehaviour
{
	public PunTeams.Team TeamToSwitchTo;
	
	public string team {get;set;}
	
	public void LoadScene(int sceneIndex)
	{
		if (photonView.isMine)
		{
			PhotonNetwork.player.SetTeam(this.TeamToSwitchTo);
		}
		Application.LoadLevel(sceneIndex);
		team = "";
	}
}
