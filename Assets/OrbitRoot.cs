﻿using UnityEngine;
using System.Collections;

public class OrbitRoot : MonoBehaviour
{
	public float speed = 5.0f;

	public void Update()
	{
		transform.Rotate(Vector3.up, Time.smoothDeltaTime * speed);
	}
}
