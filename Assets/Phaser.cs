﻿using UnityEngine;
using System.Collections;

public class Phaser : MonoBehaviour 
{
	public PlayerController player;
	public LineRenderer lineRen;
	public int playerID;
	public Vector3 hit;
	public CapsuleCollider phaserCollider;
				
	void Start()
	{
		player  = PhotonView.Find(playerID).GetComponent<PlayerController>();
	}
	
	void Update()
	{		
		this.lineRen.SetPosition(0, new Vector3(player.hardPoint.transform.position.x, 0.1f, player.hardPoint.transform.position.z));
		this.lineRen.SetPosition(1, new Vector3(hit.x, 0.1f, hit.z ));	
		phaserCollider.height = Vector3.Distance(player.hardPoint.transform.position, hit);
		phaserCollider.center = new Vector3(0,0, ((player.hardPoint.transform.position - hit)/2).magnitude);
		phaserCollider.transform.position = player.hardPoint.transform.position;
		phaserCollider.transform.LookAt(hit);
	}
		
}
