﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
	public float playerHealth;
	public float currentPlayerHealth;
	public float damage;
	public bool alive {get; set;}
	public GameObject shield;
	void Start()
	{
		alive = true;
		currentPlayerHealth = playerHealth;
	}
	void Update()
	{
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Phaser" && other.gameObject.layer  != this.gameObject.layer && shield.activeSelf == false)
		{
			damage = 33;
			currentPlayerHealth -= damage;
			other.enabled =false;
		}
		else if (other.tag == "Torp" && other.gameObject.layer  != this.gameObject.layer && shield.activeSelf == false)
		{
			damage = 45;
			currentPlayerHealth -= damage;			
			Destroy(other.gameObject);			
		}
		
	}
	
	
}
