﻿using UnityEngine;
using System.Collections;

public class RandomPlayerCam : MonoBehaviour 
{
	private GameObject player;
	
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	void FixedUpdate () 
	{
		transform.position = player.transform.position;
	}
}
