﻿using UnityEngine;
using System.Collections;

public class Respawn : Photon.MonoBehaviour 
{
	public GameObject player;
	 
	public void respawn(string shipName)
	{
		if (PhotonNetwork.player.GetTeam().ToString() == "federation")
		{
			Vector3 spawnFed = new Vector3(Random.Range(2525, 2575), 0 ,Random.Range(-2080, -2130)); 
			player = PhotonNetwork.Instantiate(shipName.ToString(), spawnFed, Quaternion.LookRotation(Vector3.forward), 0);
			player.GetComponent<PlayerController>().enabled = true;			
			player.GetComponentInChildren<shotSpawnMover>().enabled = true;
			GameObject.FindGameObjectWithTag("FederationCanvas").GetComponent<Canvas>().enabled =false;	
		}
		else if (PhotonNetwork.player.GetTeam().ToString() == "romulan")
		{			
			Vector3 spawnRom = new Vector3(Random.Range(2525, 2575), 0 ,Random.Range(1600, 1650)); 
			player = PhotonNetwork.Instantiate(shipName.ToString(), spawnRom, Quaternion.LookRotation(Vector3.back), 0);
			player.GetComponent<PlayerController>().enabled = true;				
			player.GetComponentInChildren<shotSpawnMover>().enabled = true;	
			GameObject.FindGameObjectWithTag("RomulanCanvas").GetComponent<Canvas>().enabled =false;				
		}
		GameObject gameController = PhotonNetwork.Instantiate("GameController", Vector3.zero, Quaternion.identity, 0);
		gameController.GetComponent<GameController>().enabled = true;
		gameController.GetComponent<GameController>().player = player;
	}
}
