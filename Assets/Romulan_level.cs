﻿using UnityEngine;
using System.Collections;

public class Romulan_level : Photon.MonoBehaviour 
{
	void load()
	{
		GameObject connectionStatus = GameObject.FindGameObjectWithTag("ConnectionStatus");		
		GameObject networkmanager = GameObject.FindGameObjectWithTag("NetworkManager");
		PhotonNetwork.player.SetTeam(PunTeams.Team.romulan);
		DontDestroyOnLoad(networkmanager);
		DontDestroyOnLoad(connectionStatus);
		Application.LoadLevel(2);
	}	
}
