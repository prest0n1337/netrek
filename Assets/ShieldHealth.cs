﻿using UnityEngine;
using System.Collections;

public class ShieldHealth : Photon.MonoBehaviour 
{
	public float shieldHealth;
	public float currentShieldHealth;
	public float damage;
	public PlayerController playerController;
	
	void Start()
	{
		currentShieldHealth = shieldHealth;		
	}
	
	void OnEnable()
	{
		
	}
	 void Update()
	 {
		
	 	if (currentShieldHealth <= 0.0f)
	 	{	 
	 		gameObject.GetComponent<SphereCollider>().isTrigger = false;
			playerController.gameObject.GetComponent<Collider>().isTrigger = true;			//the players collider	
			playerController.gameObject.GetComponent<Collider>().enabled = true;								
			playerController.gameObject.GetComponent<PlayerHealth>().enabled = true;			
	 		playerController.shield.SetActive(false);
	 		playerController.shieldAlive = false;
	 		playerController.audioPlayer.PlayOneShot(playerController.audioFiles[3]);	 		
	 	}
	 }
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Phaser" && other.gameObject.layer  != this.gameObject.layer)
		{
			damage = 33.0f;
			currentShieldHealth -= damage;
			other.enabled = false;
		}
		else if (other.tag == "Torp" && other.gameObject.layer  != this.gameObject.layer)
		{		
			damage = 45.0f;
			currentShieldHealth -= damage;				
			Destroy(other.gameObject);
		}
		
	}
	

}
