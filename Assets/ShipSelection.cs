﻿using UnityEngine;
using System.Collections;

public class ShipSelection : Photon.MonoBehaviour 
{
	public GameObject netManager;
	public NetworkManager netScript;
	public GameObject connectionStatus;
	public GameObject player;
	public GameObject ship;
	
	
	public void load(string ship)
	{		
		connectionStatus = GameObject.FindGameObjectWithTag("ConnectionStatus");
		netManager = GameObject.FindGameObjectWithTag("NetworkManager");
		netScript = netManager.GetComponent<NetworkManager>();
		netScript.ship = ship;
		netScript.enabled = true;
		DontDestroyOnLoad(connectionStatus);
		DontDestroyOnLoad(netManager);
		Application.LoadLevel(3);
	}	

}
