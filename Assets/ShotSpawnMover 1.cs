﻿using UnityEngine;
using System.Collections;

public class ShotSpawnMover : MonoBehaviour 
{
	private	RaycastHit hit;
	private Ray ray;
	
	void FixedUpdate()
	{			
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray ,out hit, 200000))
		{
			Vector3 mousePos = new Vector3(hit.point.x, 0.0f,hit.point.z);
			
			transform.LookAt(mousePos);
			//Debug.Log(mousePos);
		}	
	}		
}
