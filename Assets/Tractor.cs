﻿using UnityEngine;
using System.Collections;
using System;

public class Tractor : MonoBehaviour 
{
	public PlayerController player;
	public LineRenderer lineRen;
	public int playerID;
	public Vector3 hitPos;
	public CapsuleCollider tractorCollider;
	public Collider otherPlayer;
	public bool insideTractorBeam;
	public float power ;
	public int playerHit;
	public bool isPlayerHit;
	public GameObject hitPlayer;
	public PlayerHealth hitPlayerHealth;
	public PlayerHealth myHealth;
	
	
	void OnEnable()
	{
		player  = PhotonView.Find(playerID).GetComponent<PlayerController>();	
		tractorCollider.enabled = true;
		hitPlayer = PhotonView.Find(playerHit).gameObject;				
	 	hitPlayerHealth = hitPlayer.GetComponent<PlayerHealth>();
		myHealth = PhotonView.Find(playerID).GetComponent<PlayerHealth>();			
	}
/*	void Start()
	{
		player  = PhotonView.Find(playerID).GetComponent<PlayerController>();	
		tractorCollider.enabled = true;
		hitPlayer = PhotonView.Find(playerHit).gameObject;				
		hitPlayerHealth = hitPlayer.GetComponent<PlayerHealth>();
		
	}*/
	void Update()
	{
		try
		{
			lineRen.SetPosition(0, player.hardPoint.transform.position);		
		
			if(myHealth.alive == false | myHealth.alive == null | player == null)
			{
				gameObject.GetPhotonView().RPC("destroyBeam", PhotonTargets.All);			
			}		
			if(insideTractorBeam == false)
			{
				lineRen.SetPosition(1, hitPos);
				tractorCollider.height = Vector3.Distance(player.hardPoint.transform.position, hitPos) +2;
				tractorCollider.transform.LookAt(hitPos);
				tractorCollider.transform.position = player.hardPoint.transform.position;
				tractorCollider.center = new Vector3(0,0, ((player.hardPoint.transform.position - hitPos)/2).magnitude);			
			}
			else if (isPlayerHit == true)
			{
			
				hitPos = hitPlayer.transform.position;
				lineRen.SetPosition(1, otherPlayer.transform.position);				
				tractorCollider.height = Vector3.Distance(player.hardPoint.transform.position, otherPlayer.transform.position) +2;
				tractorCollider.transform.LookAt(otherPlayer.transform.position);
				tractorCollider.transform.position = player.hardPoint.transform.position;
				tractorCollider.center = new Vector3(0,0, ((player.hardPoint.transform.position - otherPlayer.transform.position)/2).magnitude);			
				otherPlayer.transform.position -= (otherPlayer.transform.position - player.hardPoint.transform.position).normalized  * power;					
			}							
		}	
		catch(MissingReferenceException)
		{
			gameObject.GetPhotonView().RPC("destroyBeam", PhotonTargets.All);
		}				
	}
	
	void OnTriggerEnter(Collider other)
	{	
		Physics.IgnoreCollision(other, PhotonView.Find(playerID).GetComponent<SphereCollider>());
		if (other.name != playerID.ToString())
		{
			insideTractorBeam = true;			
			isPlayerHit = true;
			if(other.gameObject.layer == 13 | other.gameObject.layer == 14)
			{
				hitPlayer = other.gameObject.gameObject;
			}
			else if (other.gameObject.layer == 11 | other.gameObject.layer == 12)
			{
				hitPlayer = other.gameObject;
			}
			otherPlayer = other;
			hitPos = other.transform.position;			
		}
		else if (other == null)
		{
			insideTractorBeam = false;
		}
	}
	void OnTriggerExit(Collider other)
	{
		if (other.name != playerID.ToString())
		{	
			hitPos = other.transform.position;
		}
	}	
	
	[PunRPC]
	void destroyBeam()
	{
		Destroy(this.gameObject);		
	}
}
