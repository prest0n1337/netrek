﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour
{
	public GameObject sun;
	public GameObject planet;
	public float orbitSpeed;
	public float rotateSpeed;
	
	void FixedUpdate()
	{
		planet.transform.RotateAround(sun.transform.position, Vector3.up, orbitSpeed * Time.deltaTime);
		planet.transform.Rotate(0,rotateSpeed,0 * Time.deltaTime);
	}
}

