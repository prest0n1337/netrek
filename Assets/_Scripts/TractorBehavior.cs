using UnityEngine;
using System.Collections;

public class TractorBehavior : Photon.MonoBehaviour 
{
	public PhotonView myView;
	public float power = 0.1f;
	public bool insideTractorBeam;
	public Collider otherColl;
	public PlayerController playerController;
			
	/*void Start()
	{
		Debug.Log (insideTractorBeam);		
		if (insideTractorBeam == true) 
		{ 
			myView.RPC("tractorMover", PhotonTargets.All, transform.position, power);
			Debug.Log(myView.photonView.viewID);
		}
	}*/
			
	void OnTriggerEnter(Collider other)
	{	
		//otherColl is currently used by PlayerController on the Player Object	
		otherColl = other;
		playerController.otherCollider = other;
		
		if (other.tag == "Player" | other.tag == "Torp")
		{
			insideTractorBeam = true;
		}
		else
		{
			insideTractorBeam = false;
		}
	}	
}
	