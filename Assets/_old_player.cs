﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}

public class _old_PlayerController : MonoBehaviour
{
	public Rigidbody rb;
	public float speed = 0;
	public Boundary boundary;
	public float tilt;
	public GameObject Laser;
	public GameObject Torpedo;
	public Transform shotSpawn;
	public float fireRate;
	private float nextFire = 0.0f;
	private float rotation;
	public float rotationSpeed = 150;
	public Transform playerTrans;
	private int TorpCount;
	public GameObject phaser;
	public GameObject phaser2;	
	public CapsuleCollider phaserCollider;
	
	

	void Update()
	{
		if (Input.GetButton("Orbit"))
		{	
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);		
			Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("Earth2"));
			transform.LookAt(new Vector3(hit.point.x, 0, hit.point.z));
			speed = 1;
			
		}
		if (Input.GetButton("Fire1") && Time.time > nextFire) 
		{
				
			fire (phaser);
			
		}
		
		if (Input.GetButton("Phaser") && Input.GetButtonDown("Phaser") == true)
		{
			fire(phaser2);			
		
		}
		
		if (Input.GetButton("Torpedo")|Input.GetButton("Fire2") && Time.time > nextFire)
		{
			TorpCount = GameObject.FindGameObjectsWithTag("Torp").Length;
		
			if (TorpCount <= 8)
			{
				fire (Torpedo);				
			}
			TorpCount++;
		}
		if (Input.GetButton("Detonate"))
		{
			GameObject torp = GameObject.FindGameObjectWithTag("Torp");
			if (GameObject.FindGameObjectsWithTag("Torp") != null)
			{
				Destroy(torp);
			}
			else{return;}
		}


		if (Input.GetButton ("1")) 
		{
			speed = 1;
		} 
		else if (Input.GetButton ("2")) 
		{
			speed = 5;
		}
		else if (Input.GetButton ("3")) 
		{
			speed = 10;
		}
		else if (Input.GetButton ("4")) 
		{
			speed = 15;
		}
		else if (Input.GetButton ("5")) 
		{
			speed = 20;
		}
		else if (Input.GetButton ("6")) 
		{
			speed = 25;
		}
		else if (Input.GetButton ("7")) 
		{
			speed = 30;
		}
		else if (Input.GetButton ("8")) 
		{
			speed = 35;
		}
		else if (Input.GetButton ("9")) 
		{
			speed = 40;
		}
		else if (Input.GetButton ("0")) 
		{
			speed = 0;
		}

	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");

		rb.velocity = transform.forward * speed;
		rb.position = new Vector3 
		(
			Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax),
			0.0f,
			Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
		);
		rotation += Time.deltaTime * moveHorizontal * rotationSpeed;
		rb.rotation = Quaternion.Euler(0.0f, rotation , rb.velocity.x * -tilt);
				
			
	}
	
	void fire(GameObject weapon)
	{
		nextFire = Time.time + fireRate;			
		Instantiate (weapon, shotSpawn.position, shotSpawn.rotation); 
	}
}
