﻿using UnityEngine;
using System.Collections;

public class MainCamFollower : MonoBehaviour 
{
	private GameObject player;
	private Transform Camera;
	
	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		Camera = this.transform;
	}
	
	void FixedUpdate()
	{
		follow();
	}
	void follow()
	{
		Camera.transform.position = new Vector3(player.transform.position.x, Camera.position.y, player.transform.position.z);
	}
}
