using UnityEngine;
using System.Collections;
using System;

public class PlayerController : Photon.MonoBehaviour
{
	public Camera camera;
	public Camera mapCam;
	public AudioListener audio;
	public AudioSource audioPlayer;
	public AudioClip[] audioFiles;
	public Rigidbody myRigidbody;
	public Transform myTransform;
	public PhotonView myPhotonView;
	public GameObject hardPoint;	
	public int whoFiredPhaser;
	public int whoFiredTractor;
	public int whoFiredPusher;
		
	//phaser vars
	public GameObject phaser;
	public bool phaserOn;
	
	//Torp vars
	public GameObject torp;
	
	//Shield vars
	public GameObject shield;
	public bool shieldOn;
	public bool shieldAlive;
	
	//Tractor vars
	public GameObject tractor;
	public bool tractorOn;
	public int tractorID;
	
	//Pusher vars
	public GameObject pusher;
	public bool pusherOn;
	public int pusherID;
	
		//Click Position
	public RaycastHit hit;
	public Ray ray;
	public Vector3 hitpos;
	public Vector3 hitPosPusher;
	public Vector3 hitPosPhaser;
	public Vector3 hitPosTractor;
	public bool hitPlayerPusher;
	public int hitPlayerIDPusher;
	public bool hitPlayerTractor;
	public int hitPlayerIDTractor;
	
	//map cam zoom
	public float scrollZoom;
	
	private float nextFire;
	public float fireRate;
	[Range(0,150)]
	public float playerRotationSpeed = 150;
	public float playerSpeed = 0;
	public int torpCount;


	void OnEnable()
	{
		camera.enabled = true;
		mapCam.enabled = true;
		audio.enabled =true;	
		phaserOn = false;
		tractorOn = false;
		pusherOn =false;
	}
	
	void Start()
	{		
		myPhotonView = photonView;
		camera.transform.rotation = Quaternion.Euler (90,0,0);							
		mapCam.transform.rotation = Quaternion.Euler (90,0,0);		
	}
	void Update()
	{
		shieldOn = shield.GetComponent<MeshRenderer>().enabled;
		playerSpeed = getMoveSpeed();
		
		playerInput();
		try
		{
			shieldAlive =gameObject.GetComponentInChildren<ShieldHealth>().currentShieldHealth > 0;
		}
		catch(NullReferenceException)
		{
			shieldAlive =false;
		}
	}
		
	void FixedUpdate()
	{
		move ();
	}
	
	void move()
	{
		float horizontal = Input.GetAxis("Horizontal");		
		scrollZoom = Mathf.Clamp(Input.GetAxis("Mouse ScrollWheel"), 250,250);
		
		if (Input.GetAxis("Horizontal")  != 0)
		{
			myTransform.Rotate(Vector3.up, horizontal * playerRotationSpeed  * Time.deltaTime);
			camera.transform.rotation = Quaternion.Euler (90,0,0);					
			mapCam.transform.rotation = Quaternion.Euler (90,0,0);			
		}
		myRigidbody.velocity =  myTransform.forward  * playerSpeed  * Time.deltaTime;	
	}
	void playerInput()
	{
		if (Input.GetButton("Fire1")  && Input.GetButtonDown("Fire1"))
		{
			ray = camera.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray, out hit,2000))
			hitPosPhaser = new Vector3(hit.point.x,0,hit.point.z);			
			whoFiredPhaser = photonView.viewID;
			myPhotonView.RPC("firePhaser",PhotonTargets.All, whoFiredPhaser, hardPoint.transform.position, hardPoint.transform.rotation, hitPosPhaser);	
		}
		
		if (Input.GetAxis("Mouse ScrollWheel") > 0 && mapCam.orthographicSize < 1500)
		{			
			Debug.Log("true");
			mapCam.orthographicSize += scrollZoom;			
		}
		
		if(Input.GetAxis("Mouse ScrollWheel") < 0 && mapCam.orthographicSize > 250)
		{
			mapCam.orthographicSize -= scrollZoom;						
		}
		
		if (Input.GetButton("Torpedo") | Input.GetButton("Fire2")  && Time.time > nextFire && torpCount <= 8)
		{			
			nextFire = Time.time + fireRate;
			myPhotonView.RPC("fireTorp", PhotonTargets.All, hardPoint.transform.position, hardPoint.transform.rotation, myPhotonView.viewID);			
			torpCount++;			
		}		
		
		if( Input.GetButtonDown("Tractor") && tractorOn == false)
		{
			ray = camera.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray, out hit,2000))
			{
				hitPosTractor = new Vector3(hit.point.x,0,hit.point.z);	
				if(hit.collider.gameObject.layer == 11 | hit.collider.gameObject.layer == 12 |hit.collider.gameObject.layer == 13 | hit.collider.gameObject.layer == 14)
				{
					hitPlayerTractor = true;
					hitPlayerIDTractor = hit.collider.gameObject.GetPhotonView().viewID;
				}				
			}
			whoFiredTractor = photonView.viewID;
			tractorID = tractor.GetInstanceID();
			tractorOn = true;
			myPhotonView.RPC("fireTractor",PhotonTargets.All, whoFiredTractor, hardPoint.transform.position, hardPoint.transform.rotation, hitPosTractor, tractorID, hitPlayerTractor,hitPlayerIDTractor);	
		}
		else if(Input.GetButtonDown("Tractor") && tractorOn == true)
		{			
			hitPlayerTractor = false;					
			myPhotonView.RPC("tractorOff", PhotonTargets.AllBuffered, tractorID, hitPlayerTractor);
			tractorOn = false;			
		}
		
		if( Input.GetButtonDown("Pusher") && pusherOn == false)
		{
			ray = camera.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray, out hit,2000))
			{
				hitPosPusher = new Vector3(hit.point.x,0,hit.point.z);		
				if(hit.collider.gameObject.layer == 11 | hit.collider.gameObject.layer == 12 |hit.collider.gameObject.layer == 13 | hit.collider.gameObject.layer == 14)
				{
					hitPlayerPusher = true;
					hitPlayerIDPusher = hit.collider.gameObject.GetPhotonView().viewID;				
				}
			}
			whoFiredPusher = photonView.viewID;
			pusherID = pusher.GetInstanceID();
			pusherOn = true;
			myPhotonView.RPC("firePusher",PhotonTargets.All, whoFiredPusher, hardPoint.transform.position, hardPoint.transform.rotation, hitPosPusher, pusherID, hitPlayerPusher, hitPlayerIDPusher);	
		}
		else if(Input.GetButtonDown("Pusher") && pusherOn == true)
		{			
			hitPlayerPusher = false;			
			myPhotonView.RPC("pusherOff", PhotonTargets.AllBuffered, pusherID, hitPlayerPusher = false);
			pusherOn = false;	
		}
		
		if (Input.GetButton("Shield") && Input.GetButtonDown("Shield") && shieldOn == false && shieldAlive == true)
		{
			myPhotonView.RPC("shieldsUp", PhotonTargets.AllBuffered);
			shieldOn = true;
		}
		else if (Input.GetButton("Shield") && Input.GetButtonDown("Shield") && shieldOn == true)
		{
			myPhotonView.RPC("shieldsDown", PhotonTargets.AllBuffered);		
			shieldOn = false;	
		}
		
		if(Input.GetButton("Detonate") && torpCount > 0)
		{
			myPhotonView.RPC("removeTorp", PhotonTargets.All, myPhotonView.viewID);
			torpCount--;
			Debug.Log(torpCount);
		}
		
	}
	
	[PunRPC]
	void pusherOff(int id, bool isHit)
	{
		pusher.GetComponent<Pusher>().isPlayerHit = isHit;		
		GameObject pusherNet = GameObject.Find(id.ToString()+"(Clone)");
		Destroy(pusherNet);
	}
	
	[PunRPC]
	void tractorOff(int id, bool isHit)
	{
		tractor.GetComponent<Tractor>().isPlayerHit = isHit;		
		GameObject tractorNet = GameObject.Find(id.ToString()+"(Clone)");
		Destroy(tractorNet);
	}
			
	[PunRPC]
	void fireTorp(Vector3 start, Quaternion rotation, int viewID)
	{
		torp.name = viewID.ToString();
		
		if(PhotonView.Find(viewID).gameObject.layer == 11)
		{
			torp.layer = 8;	
					
		}
		else if (PhotonView.Find(viewID).gameObject.layer == 12)
		{
			torp.layer = 9;
		}
		GameObject.Instantiate(torp, start, rotation);
	}
	
	[PunRPC]
	void firePusher(int playerID, Vector3 position, Quaternion rotation, Vector3 hitposition, int pusherId, bool isHit, int hitWho)
	{
		whoFiredPusher = playerID;
		pusher.name = pusherId.ToString();
		pusher.GetComponent<Pusher>().playerID = playerID;
		pusher.GetComponent<Pusher>().enabled = true;
		pusher.GetComponent<Pusher>().hitPos = hitposition;
		pusher.GetComponent<Pusher>().isPlayerHit = isHit;
		pusher.GetComponent<Pusher>().playerHit = hitWho;		
		GameObject.Instantiate(pusher, position, rotation);		
	}
	
	[PunRPC]
	void fireTractor(int playerID, Vector3 position, Quaternion rotation, Vector3 hitposition, int tractorId, bool isHit, int hitWho)
	{
		whoFiredTractor = playerID;
		tractor.name = tractorId.ToString();
		tractor.GetComponent<Tractor>().playerID = playerID;
		tractor.GetComponent<Tractor>().enabled = true;
		tractor.GetComponent<Tractor>().hitPos= hitposition;
		tractor.GetComponent<Tractor>().isPlayerHit = isHit;
		tractor.GetComponent<Tractor>().playerHit = hitWho;		
		GameObject.Instantiate(tractor, position, rotation);		
	}
	
	[PunRPC]
	void firePhaser(int playerID, Vector3 position, Quaternion rotation, Vector3 hitposition)
	{
		whoFiredPhaser = playerID;
		phaser.GetComponent<Phaser>().playerID = playerID;
		phaser.GetComponent<Phaser>().hit = hitposition;
		if(PhotonView.Find(playerID).gameObject.layer == 11)
		{
			phaser.layer = 8;
			
		}
		else if (PhotonView.Find(playerID).gameObject.layer == 12)
		{
			phaser.layer = 9;
		}
		GameObject.Instantiate(phaser, position, rotation);	
	}
	
	[PunRPC]
	void shieldsUp()
	{		
		shield.GetComponent<SphereCollider>().enabled = true;
		shield.GetComponent<MeshRenderer>().enabled = true;		
		audioPlayer.PlayOneShot(audioFiles[2]);
		gameObject.GetComponent<Collider>().isTrigger = false;		
	}
	
	[PunRPC]
	void shieldsDown()
	{
		shield.GetComponent<SphereCollider>().enabled = false;
		shield.GetComponent<MeshRenderer>().enabled = false;
		audioPlayer.PlayOneShot(audioFiles[3]);
		gameObject.GetComponent<Collider>().isTrigger = true;		
	}	
	
	[PunRPC]
	void removeTorp(int id)
	{
		GameObject t = GameObject.Find (id.ToString () + "(Clone)");
		Destroy (t);
	}
	
	
	float getMoveSpeed()
	{
			
		if (Input.GetKey(KeyCode.Alpha1))
		{
			playerSpeed = 300;
			playerRotationSpeed = 150;
			return playerSpeed;
		}
		if (Input.GetKey(KeyCode.Alpha2))
		{
			playerSpeed = 500;
			playerRotationSpeed = 120;

			return playerSpeed;			
		}
		if (Input.GetKey(KeyCode.Alpha3))
		{
			playerSpeed = 700;
			playerRotationSpeed = 105;

			return playerSpeed;			
		}
		if (Input.GetKey(KeyCode.Alpha4))
		{
			playerSpeed = 900;
			playerRotationSpeed = 90;

			return playerSpeed;			
		}
		if (Input.GetKey(KeyCode.Alpha5))
		{
			playerSpeed = 1100;
			playerRotationSpeed = 75;

			return playerSpeed;			
		}
		if (Input.GetKey(KeyCode.Alpha6))
		{
			playerSpeed = 130000;
			playerRotationSpeed = 60;

			return playerSpeed;			
		}
		if (Input.GetKey(KeyCode.Alpha7))
		{
			playerSpeed = 1500;
			playerRotationSpeed = 45;

			return playerSpeed;			
		}
		if (Input.GetKey(KeyCode.Alpha8))
		{
			playerSpeed = 1700;
			playerRotationSpeed = 30;

			return playerSpeed;			
		}
		if (Input.GetKey(KeyCode.Alpha9))
		{
			playerSpeed = 1900;
			playerRotationSpeed = 15;
			audioPlayer.volume = 0.6f;
			audioPlayer.PlayOneShot(audioFiles[4]);
			return playerSpeed;			
		}
		if (Input.GetKey(KeyCode.Alpha0))
	    {
			playerSpeed = 0;
			audioPlayer.volume = 0.4f;
			audioPlayer.PlayOneShot(audioFiles[5]);
			return playerSpeed;			
		}
		return playerSpeed;
	}
		
}
	
	