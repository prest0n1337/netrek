﻿using UnityEngine;
using System.Collections;

public class MainCameraFollowPlayerOLD : MonoBehaviour 
{
	 private GameObject Player;
	 private GameObject MainCamera;
	 private Vector3 newPosition;
	 
	 void Awake()
	 {
	 	MainCamera = GameObject.FindGameObjectWithTag("MainCamera");
	 	Player = GameObject.FindGameObjectWithTag("Player");	 	
	 }
	 void Update()
	 {
		newPosition = new Vector3(Player.transform.position.x, MainCamera.transform.position.y, Player.transform.position.z);
	 	MainCamera.transform.position = newPosition;
	 }
	
}
