﻿using UnityEngine;
using System.Collections;

public class TTL : MonoBehaviour
{
	public float TimeToLive = 1f;
	
	void OnEnable() 
	{
		StartCoroutine(remove()); 
	}
	IEnumerator remove()
	{ 
		yield return new WaitForSeconds(TimeToLive); 
		//playerController.phaserLR.enabled = false;
		Destroy(this.gameObject);
	}
}
