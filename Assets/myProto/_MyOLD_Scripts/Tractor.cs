﻿using UnityEngine;
using System.Collections;

public class Tractor : MonoBehaviour 
{
	public LineRenderer lineRen;
	RaycastHit hit;
	Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	public Vector3 endPoint;
	private Vector3 hardPoint;
	private GameObject weaponSpawn;
	private CapsuleCollider tractorCollider;
	private Vector3 colliderCenter;
	private Collider objCollider;
	private string enTag;
	
	void Start()
	{
		tractorCollider =  GetComponent<CapsuleCollider>();
		
	}
	void FixedUpdate()
	{		
		weaponSpawn = GameObject.FindGameObjectWithTag("WeaponSpawn");
		if(Physics.Raycast(ray, out hit, 20000, LayerMask.GetMask("Background")))
		{	
			hardPoint = new Vector3(weaponSpawn.transform.position.x,weaponSpawn.transform.position.y,weaponSpawn.transform.position.z);
			if (enTag == "Enemy")
			{ 		
				endPoint = objCollider.transform.position;	
				tractorCollider.height = ((objCollider.transform.position - hardPoint)).magnitude;							
				colliderCenter = new Vector3(0,0,((objCollider.transform.position - hardPoint)/2).magnitude);
				
			}
			else if(enTag == null)
			{				
				endPoint = new Vector3(hit.point.x, 0, hit.point.z);
				tractorCollider.height = ((hit.point - hardPoint)).magnitude;		
				colliderCenter = new Vector3(0,0,((hit.point - hardPoint)/2).magnitude);
				
				
			}
			lineRen.SetPosition(0, hardPoint);				
			lineRen.SetPosition(1, endPoint); 
			tractorCollider.center = colliderCenter ;
			tractorCollider.transform.position = hardPoint;
			tractorCollider.transform.LookAt(endPoint);								
			  
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy")
		{
			lineRen.SetPosition(1, other.transform.position);
			endPoint = other.transform.position;
			objCollider = other;
			enTag = "Enemy";
		}
		
	}
	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Enemy")
		{
			lineRen.SetPosition(1, other.transform.position);
			endPoint = other.transform.position;
			enTag = null;
		}
			
	}
		
	
	
	
	
	
	
	
	
	
	
}