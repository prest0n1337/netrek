﻿using UnityEngine;
using System.Collections;

public class shotSpawnMover : MonoBehaviour 
{

	private Ray ray; 
	private RaycastHit hit;
	private Camera cam;
	PlayerController playerController;

	void OnEnable()
	{
		playerController = GetComponentInParent<PlayerController>();
	}
	void Update()
	{
		cam = playerController.camera;
		ray = cam.ScreenPointToRay(Input.mousePosition); // Camera.current.ScreenPointToRay(Input.mousePosition);
	
		
		if(Physics.Raycast(ray, out hit, 2000))
		{
			Vector3 mousPos = new Vector3(hit.point.x, 0.0f, hit.point.z);
			transform.LookAt(mousPos);
		}
	}
}