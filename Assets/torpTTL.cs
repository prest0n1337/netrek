﻿using UnityEngine;
using System.Collections;

public class torpTTL : MonoBehaviour 
{
	public PlayerController playerController;
	public float TimeToLive;
	public int viewId; 
	
	void OnEnable() 
	{
		viewId = int.Parse(this.name.Remove(4));
		StartCoroutine(remove()); 
		playerController = PhotonView.Find(viewId).GetComponent<PlayerController>();
	}
	
	IEnumerator remove()
	{	 
		yield return new WaitForSeconds(TimeToLive); 
		playerController.torpCount -= 1;
		Destroy(this.gameObject);
	}
		
}
